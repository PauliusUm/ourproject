package io.startup.bmicalculator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.rey.material.widget.Button;
import com.rey.material.widget.EditText;
import com.rey.material.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity {

    @BindView (R.id.weightField)
    EditText wField;

    @BindView (R.id.heightField)
    EditText hField;

    @BindView (R.id.emailField)
    EditText eField;

    @BindView(R.id.resultsButton)
    Button rButton;

    @BindView (R.id.textViewField)
    TextView tvField;


    public static boolean isTextFieldsEmpty(List<EditText> texts){

        for (int i = 0; i < texts.size(); i++){
            if (isTextFieldEmpty(texts.get(i))){
                return true;
            }
        }

//        for(EditText text : texts){
//
//            if(isTextFieldEmpty(text)){
//                return true;
//            }
//
//        } // Antras variantas

        return false;

    }


    public static boolean isTextFieldEmpty(EditText text){

        if (text != null){
            if (text.getText().toString().isEmpty()){
                return true;
            }
        }
        return false;
    }

    public static String getTextFromField(EditText text){
        return text.getText().toString();

    }

    public static float getFloatFromField(EditText float12){
        return Float.parseFloat(float12.getText().toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
//        rButton = (Button)findViewById(R.id.resultsButton);
        if(rButton == null){
            Log.d("TAG","Buttonas yra nullas.");
        }

        rButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<EditText> allOfThis = new ArrayList<>();
                allOfThis.add(wField);
                allOfThis.add(hField);
                allOfThis.add(eField);


                if (isTextFieldsEmpty(allOfThis)) {

                    tvField.setText("Please fill all the fields");
                    return;

                }

                Float weight1 = getFloatFromField(wField);
                Float height1 = getFloatFromField(hField);
                float BMI = weight1 / (height1 * height1);



                final User user = new User(getTextFromField(eField), getFloatFromField(wField), getFloatFromField(hField),BMI);
                enterNewActivity(user);


            }
        });
    }

        void enterNewActivity(User user){
        if (user != null){

            String json = new Gson().toJson(user, User.class);
            Intent intent = new Intent(this,ShowRoom.class);
            intent.putExtra(Constants.USER_JSON_KEY,json);
            startActivity(intent);


        }
    }




 }

