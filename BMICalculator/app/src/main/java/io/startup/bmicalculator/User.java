package io.startup.bmicalculator;

/**
 * Created by Edva on 1/14/2017.
 */

public class User {

    private String email;
    private Float weight;
    private Float height;
    private Float bMI;

    public User(String email, Float weight, Float height, Float bMI){
        this.email = email;
        this.weight = weight;
        this.height = height;
        this.bMI = bMI;

    }

    public Float getbMI() {
        return bMI;
    }

    public void setbMI(Float bMI) {
        this.bMI = bMI;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }
}
