package io.startup.bmicalculator;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShowRoom extends AppCompatActivity {

    @BindView (R.id.textViewField)
    TextView tvField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_room);
        ButterKnife.bind(this);
        String receivedJson = getIntent().getExtras().getString(Constants.USER_JSON_KEY);
        User receivedUser = new Gson().fromJson(receivedJson, User.class);
        tvField.setText(String.valueOf(receivedUser.getbMI()));








    }
}
